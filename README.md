# Accounts API

This projects is a sample project for Pismo. This project is focused on accounts.

## Application configuration
 - Spring / Groovy
 - Swagger ([LOCAL](http://localhost:8181/swagger-ui.html#/))
 - PostgreSql

## How to Run

It's necessary to have Java 8 and Docker installed

 1. `docker-compose up --build -d`
 2. `./gradlew bootRun`
 
## IMPORTANT
   
   All endpoints and how to use can be found on [SWAGGER](http://localhost:8181/swagger-ui.html#/)

## How to Test

 1. `docker-compose up --build -d`;
 2. `./gradlew check integrationTest`.
    1. `check`: run checkstyle and unit tests
    2. `integrationTest`: run integration tests
