package com.pismo.accounts.fixtures.domain

import com.pismo.accounts.domain.Account
import spock.lang.Specification

class AccountSpec extends Specification{

    def "should add from credit limit"() {
        given:
        BigDecimal initialCredit = new BigDecimal(1000)
        Account account = new Account(availableCreditLimit: initialCredit)

        and:
        BigDecimal toAdd = new BigDecimal(500)

        when:
        account.addCredit(toAdd)

        then:
        account.availableCreditLimit == initialCredit.add(toAdd)
    }

    def "should keep credit limit"() {
        given:
        BigDecimal initialCredit = new BigDecimal(1000)
        Account account = new Account(availableCreditLimit: initialCredit)

        when:
        account.addCredit(null)

        then:
        account.availableCreditLimit == initialCredit
    }

    def "should add from Withdrawal Limit"() {
        given:
        BigDecimal initialCredit = new BigDecimal(1000)
        Account account = new Account(availableWithdrawalLimit: initialCredit)

        and:
        BigDecimal toAdd = new BigDecimal(500)

        when:
        account.addWithdrawalLimit(toAdd)

        then:
        account.availableWithdrawalLimit == initialCredit.add(toAdd)
    }

    def "should keep Withdrawal Limit"() {
        given:
        BigDecimal initialCredit = new BigDecimal(1000)
        Account account = new Account(availableWithdrawalLimit: initialCredit)

        when:
        account.addWithdrawalLimit(null)

        then:
        account.availableWithdrawalLimit == initialCredit
    }
}
