package com.pismo.accounts.fixtures

import br.com.six2six.fixturefactory.Fixture
import br.com.six2six.fixturefactory.Rule
import br.com.six2six.fixturefactory.loader.TemplateLoader
import com.pismo.accounts.domain.Account

import java.math.MathContext

class AccountFixture implements TemplateLoader {

  static final String TO_CREATE = "to-create"

  @Override
  void load() {
    Fixture.of(Account.class).addTemplate(TO_CREATE, new Rule() {
      {
        add("availableCreditLimit", random(BigDecimal.class, new MathContext(2)))
        add("availableWithdrawalLimit", random(BigDecimal.class, new MathContext(2)))
      }
    })
  }

}
