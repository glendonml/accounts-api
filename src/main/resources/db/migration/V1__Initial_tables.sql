create table account (
  id bigserial primary key,
  available_credit_limit numeric(15,2) not null,
  available_withdrawal_limit numeric(15,2) not null
);