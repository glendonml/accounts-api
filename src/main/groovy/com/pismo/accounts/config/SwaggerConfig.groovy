package com.pismo.accounts.config;

import com.pismo.accounts.controller.v1.AccountController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
class SwaggerConfig {

  @Bean
  Docket transactionsApi() {
    new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage(
            AccountController.class.getPackage().getName()))
            .build()
            .ignoredParameterTypes(groovy.lang.MetaClass)
  }

  private ApiInfo metaData() {
    ApiInfo apiInfo = new ApiInfo(
        "AccountsAPI",
        "Sample application for Pismo",
        "1.0",
        "Terms of service",
        "Glendon Leitão",
        "Apache License Version 2.0",
        "https://www.apache.org/licenses/LICENSE-2.0")
  }
}
