package com.pismo.accounts.resource.v1;

import com.fasterxml.jackson.annotation.JsonProperty

class UpdateBalanceRequest {

  @JsonProperty(value = "available_credit_limit")
  private Amount availableCreditLimit

  @JsonProperty(value = "available_withdrawal_limit")
  private Amount availableWithdrawalLimit

  class Amount {
    BigDecimal amount
  }

}
