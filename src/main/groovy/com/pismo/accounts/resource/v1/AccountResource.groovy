package com.pismo.accounts.resource.v1

import com.fasterxml.jackson.annotation.JsonProperty
import com.pismo.accounts.domain.Account

class AccountResource {

  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  Long id

  @JsonProperty(value = "available_credit_limit", required = true)
  BigDecimal availableCreditLimit

  @JsonProperty(value = "available_withdrawal_limit", required = true)
  BigDecimal availableWithdrawalLimit

  def generateAccount() {
    new Account(availableCreditLimit: availableCreditLimit, availableWithdrawalLimit: availableWithdrawalLimit)
  }

  static def create(Account account) {
    new AccountResource(id: account.id, availableCreditLimit: account.availableCreditLimit,
            availableWithdrawalLimit: account.availableWithdrawalLimit)
  }
}
