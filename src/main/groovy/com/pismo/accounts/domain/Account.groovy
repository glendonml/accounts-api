package com.pismo.accounts.domain;

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.validation.constraints.NotNull

@Entity
class Account {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id

  @NotNull
  BigDecimal availableCreditLimit

  @NotNull
  BigDecimal availableWithdrawalLimit

  def addCredit(creditLimit) {
    availableCreditLimit = creditLimit ?
            availableCreditLimit.add(creditLimit) : availableCreditLimit
  }

  def addWithdrawalLimit(withdrawalLimit) {
    availableWithdrawalLimit = withdrawalLimit ?
            availableWithdrawalLimit.add(withdrawalLimit) : availableWithdrawalLimit
  }
}
