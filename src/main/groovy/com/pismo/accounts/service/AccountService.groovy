package com.pismo.accounts.service

import com.pismo.accounts.domain.Account
import com.pismo.accounts.repository.AccountRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
class AccountService {

    @Autowired
    AccountRepository accountRepository

    Account updateBalance(Long id, BigDecimal creditLimit,
                            BigDecimal withdrawalLimit) {
        Account account = accountRepository.findOne(id)
        account.addCredit(creditLimit)
        account.addWithdrawalLimit(withdrawalLimit)

        accountRepository.save(account)
    }
}
