package com.pismo.accounts.repository

import com.pismo.accounts.domain.Account
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface AccountRepository extends CrudRepository<Account, Long> {
}