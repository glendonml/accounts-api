package com.pismo.accounts

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class AccountsApplication {

	static void main(String[] args) {
		SpringApplication.run AccountsApplication, args
	}
}
