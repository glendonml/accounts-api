package com.pismo.accounts.controller.v1

import com.pismo.accounts.repository.AccountRepository
import com.pismo.accounts.resource.v1.AccountResource
import com.pismo.accounts.resource.v1.UpdateBalanceRequest
import com.pismo.accounts.service.AccountService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = "/v1/accounts")
@Api(value="accounts", description="Account operations")
class AccountController {

    @Autowired
    AccountService accountService

    @Autowired
    AccountRepository accountRepository

    @PatchMapping("/{id}")
    @ApiOperation(value = "Update available credit for account")
    ResponseEntity updateBalance(@PathVariable("id") Long id,
                                   @RequestBody UpdateBalanceRequest accountRequest) {
        accountService.updateBalance(id, accountRequest?.availableCreditLimit?.amount,
                accountRequest?.availableWithdrawalLimit?.amount)

        ResponseEntity.ok().build()
    }

    @PostMapping
    @ApiOperation(value = "Create an account")
    ResponseEntity create(@RequestBody AccountResource accountResource) {
        def created = AccountResource.create(accountRepository.save(accountResource?.generateAccount()))
        ResponseEntity.ok(created)
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get an account by Id")
    ResponseEntity get(@PathVariable("id") Long id) {
        def found = AccountResource.create(accountRepository.findOne(id))
        ResponseEntity.ok(found)
    }


}
