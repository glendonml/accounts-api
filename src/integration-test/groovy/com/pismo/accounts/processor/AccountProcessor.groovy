package com.pismo.accounts.processor

import br.com.six2six.fixturefactory.processor.Processor
import com.pismo.accounts.repository.AccountRepository

class AccountProcessor implements Processor {

    def accountRepository

    AccountProcessor(AccountRepository accountRepository) {
        this.accountRepository = accountRepository
    }

    @Override
    void execute(result) {
        accountRepository.save(result)
    }
}
