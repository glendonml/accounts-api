package com.pismo.accounts.repository

import br.com.six2six.fixturefactory.Fixture
import com.pismo.accounts.BaseSpec
import com.pismo.accounts.domain.Account
import com.pismo.accounts.fixtures.AccountFixture
import com.pismo.accounts.fixtures.FixtureLoader
import com.pismo.accounts.processor.AccountProcessor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional

@Transactional
class AccountRepositorySpec extends BaseSpec {

    @Autowired
    AccountRepository accountRepository

    AccountProcessor accountProcessor

    def setup() {
        FixtureLoader.loadTemplates()
        accountProcessor = new AccountProcessor(accountRepository)
    }

    def "should create a account"() {
        given: "a new account"
        def account = Fixture.from(Account.class).gimme(AccountFixture.TO_CREATE)

        when:
        accountRepository.save(account)
        Account savedAccount =  accountRepository.findOne(account.id)

        then:
        savedAccount != null
    }

    def "should update a account"() {
        given: "a account"
        Account savedAccount = Fixture.from(Account.class).uses(accountProcessor).gimme(AccountFixture.TO_CREATE)

        when:"update the amount and balance"
        savedAccount.availableCreditLimit = BigDecimal.TEN
        savedAccount.availableWithdrawalLimit = BigDecimal.TEN
        accountRepository.save(savedAccount)
        Account updatedAccount =  accountRepository.findOne(savedAccount.id)

        then:
        updatedAccount.availableCreditLimit == BigDecimal.TEN
        updatedAccount.availableWithdrawalLimit == BigDecimal.TEN
    }

    def "should delete a account"() {
        given: "a account"
        Account savedAccount = Fixture.from(Account.class).uses(accountProcessor).gimme(AccountFixture.TO_CREATE)

        when:
        accountRepository.delete(savedAccount)
        Account deletedAccount =  accountRepository.findOne(savedAccount.id)

        then:
        deletedAccount == null
    }

}